// Chatbot.js
import React, { useState, useRef, useEffect } from "react";
import axios from "axios";
import ReactCountryFlag from "react-country-flag";
import "./Chatbot.css";
import { useTranslation } from "react-i18next";
import i18n from "../utils/i18n";
import { ThreeDots } from "react-loader-spinner";

const Chatbot = () => {
  const { t, i18n } = useTranslation();
  const [isOpen, setIsOpen] = useState(false);
  const [userInput, setUserInput] = useState("");
  const [chatMessages, setChatMessages] = useState([]);
  const [lang, setLang] = useState(navigator.language.split("-")[0]);
  const [isLoading, setIsLoading] = useState(false);
  const [token, setToken] = useState("");

  const toggleChat = () => {
    setIsOpen(!isOpen);
    if (chatMessages.length === 0) {
      setIsLoading(true);
      setTimeout(() => {
        setChatMessages((prevMessages) => [
          ...prevMessages,
          { text: t("hello"), type: "bot" },
          { text: t("changeLanguage"), type: "bot" },
          { text: t("rockets"), type: "bot", clickable: true },
          { text: t("tesla"), type: "bot", clickable: true },
          { text: t("starlink"), type: "bot", clickable: true },
          { text: t("iss"), type: "bot", clickable: true },
        ]);
        setIsLoading(false);
      }, 500);
    }
  };

  const handleInputChange = (e) => {
    setUserInput(e.target.value);
  };

  const handleLangChange = (language) => {
    setLang(language);
    setChatMessages((prevMessages) => [
      ...prevMessages,
      { text: t("language_changed") + language, type: "bot" },
    ]);
  };

  const handleSendMessage = () => {
    if (!userInput) return;

    if (token === "" && localStorage.getItem("token") !== null) {
      setToken(localStorage.getItem("token"));
    }

    setChatMessages((prevMessages) => [
      ...prevMessages,
      { text: userInput, type: "user" },
    ]);
    setUserInput("");

    axios
      .post("http://127.0.0.1:8000/api/ask", {
        token: token,
        lang: lang,
        question: userInput,
      })
      .then((response) => {
        localStorage.setItem("token", response.data.token);
        setChatMessages((prevMessages) => [
          ...prevMessages,
          { text: response.data.response, type: "bot" },
        ]);
      })
      .catch((error) => {
        setChatMessages((prevMessages) => [
          ...prevMessages,
          { text: t("error_server"), type: "bot" },
        ]);
      });
  };

  const chatContainerRef = useRef(null);
  useEffect(() => {
    if (chatContainerRef.current) {
      chatContainerRef.current.scrollTop =
        chatContainerRef.current.scrollHeight;
    }
  }, [chatMessages]);

  return (
    <div className={`chatbot-container ${isOpen ? "open" : ""}`}>
      <div className="chatbot-icons-container">
        <div className="chatbot-icon" onClick={toggleChat}>
          {/* Icône du chatbot */}
          <img
            src="images/chat-bot.png"
            alt="Chatbot"
            className="chatbot-svg"
          />
        </div>
        {isOpen && (
          /* Icône de changement de langue (à droite) */
          <div>
            <ReactCountryFlag
              countryCode={"GB"}
              svg
              style={{
                width: "50px",
                height: "50px",
                borderRadius: "25%",
                marginLeft: "10px",
                cursor: "pointer",
              }}
              onClick={() => handleLangChange("en")}
              title={"GB"}
            />
          </div>
        )}
        {isOpen && navigator.language.split("-")[1] !== "GB" && (
          /* Icône de changement de langue (à droite) */
          <div>
            <ReactCountryFlag
              countryCode={navigator.language.split("-")[1]}
              svg
              style={{
                width: "50px",
                height: "50px",
                borderRadius: "25%",
                marginLeft: "10px",
                cursor: "pointer",
              }}
              onClick={() => handleLangChange(navigator.language.split("-")[0])}
              title={navigator.language.split("-")[1]}
            />
          </div>
        )}
      </div>
      {isOpen && (
        <div className="chatbot-content">
          <div className="chat-messages" ref={chatContainerRef}>
            {isLoading ? (
              <ThreeDots
                visible={true}
                height="50"
                width="50"
                color="#007bff"
                radius="9"
                ariaLabel="three-dots-loading"
                wrapperStyle={{}}
                wrapperClass=""
              />
            ) : (
              chatMessages.map((message, index) => (
                <div
                  key={index}
                  className={`chat-message ${
                    message.type === "bot" ? "bot-content" : "user-content"
                  }`}
                  onClick={() => {
                    if (message.clickable) {
                      setUserInput(message.text);
                    }
                  }}
                  style={
                    message.clickable
                      ? { cursor: "pointer", textDecoration: "underline" }
                      : {}
                  }
                >
                  <span className={message.type === "bot" ? "bot-text" : "user-text"}>
                  {message.type === "bot" &&
                  (index === 0 || chatMessages[index - 1]?.type === "user") ? (
                    <b> SpaceBot : </b>
                  ) : (
                    ""
                  )}
                  {message.text}</span>
                </div>
              ))
            )}
          </div>
          {/* Champ de saisie et bouton d'envoi */}
          <div className="bottom-bar">
            <input
              type="text"
              value={userInput}
              onChange={handleInputChange}
              placeholder={t("placeholder_input")}
              className="input-field"
            />
            <button onClick={handleSendMessage} className="button button1">
              {t("text_button")}
            </button>
          </div>
        </div>
      )}
    </div>
  );
};

export default Chatbot;
