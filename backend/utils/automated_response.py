import requests
from datetime import datetime
from models.BotResponse import BotResponse
from utils.error_handler import handle_error
from utils.translate import translate

fr_responses = {
    "next_launch": "Le prochain lancement de fusée est le",
    "iss_position": "Vous pouvez la position actuelle de l'ISS a cette adresse",
    "tesla_position": "Vous pouvez la position actuelle de la Tesla Roadster a cette adresse",
    "starlink_position": "Fin 2022, le nombre satellites Starlink en orbite autour de la Terre était de"
}

en_responses = {
    "next_launch": "The next rocket launch is",
    "iss_position": "You can check ISS position at this address",
    "tesla_position": "You can check Tesla Roadster position at this address",
    "starlink_position": "End of 2022, the number of Starlink satellites in orbit around the Earth was"
}


def format_response(lang, response_type, response):
    if lang != "fr" or lang != "en":
        trad_response = translate(en_responses[response_type], "en", lang)
        full_response = trad_response + f" {response}"
        return full_response
    if lang == "fr":
        full_response = fr_responses[response_type] + f" {response}"
        return full_response
    return en_responses[response_type] + f" {response}"


def get_next_launch(lang):
    try:
        next_launches_list = requests.get("https://fdo.rocketlaunch.live/json/launches/next/5").json()
        launch_date = "No launch date found"
        for launch in next_launches_list["result"]:
            if launch["provider"]["name"] == "SpaceX":
                launch_date = launch["t0"]
                launch_date = datetime.strptime(launch_date, "%Y-%m-%dT%H:%MZ")
                launch_date = launch_date.strftime("%B %d, %Y, %H:%M UTC")
                break
        return format_response(lang, "next_launch", launch_date)
    except BaseException as e:
        return handle_error(e)


def get_iss_position(lang):
    try:
        current_location = requests.get("http://api.open-notify.org/iss-now.json").json()
        latitude = current_location["iss_position"]["latitude"]
        longitude = current_location["iss_position"]["longitude"]
        link = f"https://www.google.com/maps/search/?api=1&query={latitude},{longitude}"
        return format_response(lang, "iss_position", link)
    except BaseException as e:
        return handle_error(e)


def get_tesla_position(lang):
    try:
        return format_response(lang, "tesla_position", "https://www.whereisroadster.com/")
    except BaseException as e:
        return handle_error(e)


def get_starlink_count(lang):
    try:
        all_starlinks = requests.get("https://api.spacexdata.com/v4/starlink").json()
        sat_number = len(all_starlinks)
        return format_response(lang, "starlink_position", sat_number)
    except BaseException as e:
        return handle_error(e)


def get_auto_response(lang, question):
    if question == "next_launch":
        return get_next_launch(lang)
    if question == "iss_position":
        return get_iss_position(lang)
    if question == "tesla_position":
        return get_tesla_position(lang)
    if question == "starlink_position":
        return get_starlink_count(lang)
