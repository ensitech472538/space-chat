import os

from dotenv import load_dotenv
from pymongo import MongoClient


async def db_connect():
    load_dotenv()
    client = MongoClient(f"mongodb+srv://{os.environ.get('MONGO_USER')}:{os.environ.get('MONGO_PASS')}@cluster0.knl089b.mongodb.net/?retryWrites=true&w=majority")
    return client
