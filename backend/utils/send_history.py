from models.Ask import Ask
from models.BotResponse import BotResponse
from models.History import History, Exchange
from utils.database import db_connect
from uuid import uuid4

"""
This function will send the user question and the bot response to the database in an history

token: str -> the token of the user, if it is the first time, it will be None
question: Ask -> the question the user asked
response: BotResponse -> the response of the bot

the function will return the token of the user, that will be usefull for higher level functions
"""
async def send_to_history(token, question: Ask, response: BotResponse):
    try:
        db = await db_connect()
        # If not token, we create a new history
        if not token:
            token = str(uuid4())
            history = History(token=token, history=[Exchange(question=question, response=response)])
            db['chatbot'].history.insert_one(history.model_dump())
            return token
        # If token, we need to update the history by getting it then inserting the new response in the history field
        db['chatbot'].history.update_one(
            {"token": token},
            {"$push": {"history": Exchange(question=question, response=response).to_dict()}}
        )
        return token

    except BaseException as e:
        print(e)
