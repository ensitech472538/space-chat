from fastapi import APIRouter
from starlette.status import HTTP_200_OK

from models.Ask import Ask
from models.BotResponse import BotResponse
from utils.Mistral import Mistral_Chat
from utils.check_words import check_words
from utils.automated_response import get_auto_response
from utils.error_handler import handle_error
from utils.send_history import send_to_history

router = APIRouter(tags=["ask"])

"""
main route for the ask router, all will begin from here
complete path: /api/ask
"""


@router.post("/", status_code=HTTP_200_OK)
async def user_message(message: Ask):
    try:
        lang = message.lang
        # Trigger question analysis on french words
        words = check_words(lang, message.question)
        # Redirect to AI API if no enough words were found
        if not words:
            ai_response = Mistral_Chat(message.question)
            response = BotResponse(response=ai_response)
            token = await send_to_history(message.token, message, response)
            response.token = token
            return response

        response = BotResponse(response=get_auto_response(lang, words))
        token = await send_to_history(message.token, message, response)
        response.token = token
        return response

    except BaseException as e:
        return handle_error(e)
