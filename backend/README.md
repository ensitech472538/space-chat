# How to run backend

## Python setup

- Install python 3.8.0 (at least)
- Install pipenv (https://docs.python-guide.org/dev/virtualenvs/)

Pipenv will be useful to manage packages like npm does.  

If you have to install packages, your terminal must point to de backend directory, not root one.

## Install dependencies
```
pipenv install
```

## Run the server
```
uvicorn main:app --reload
```

The server should restart automatically when you change the backend code.

## About AI

The bot use Mistral as AI top answer many questions.
Please ensure, to have a Mistral API key in your .env file. The .env file must be in the backend directory.